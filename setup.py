
import sys
import os
import re
import sysconfig
import tempfile
from setuptools import setup
from distutils.core import Extension
from distutils.spawn import find_executable
from distutils.ccompiler import new_compiler

def get_platform():
    # cross build
    if "_PYTHON_HOST_PLATFORM" in os.environ:
        return os.environ["_PYTHON_HOST_PLATFORM"]
    # Get value of sys.platform
    if sys.platform.startswith('osf1'):
        return 'osf1'
    return sys.platform

def configure_ext(build_temp):
    exts = []

    compiler = new_compiler()
    lib_dirs = compiler.library_dirs + [
        '/lib64', '/usr/lib64',
        '/lib', '/usr/lib',
    ]
    inc_dirs = compiler.include_dirs + ['/usr/include']

    cross_compiling = "_PYTHON_HOST_PLATFORM" in os.environ
    do_readline = compiler.find_library_file(lib_dirs, 'readline')
    host_platform = get_platform()

    readline_termcap_library = ""
    tmpfile = os.path.join(build_temp, 'readline_termcap_lib')
    if not os.path.exists(build_temp):
        os.makedirs(build_temp)
    # Determine if readline is already linked against curses or tinfo.
    if do_readline:
        if cross_compiling:
            ret = os.system("%s -d %s | grep '(NEEDED)' > %s" \
                            % (sysconfig.get_config_var('READELF'),
                               do_readline, tmpfile))
        elif find_executable('ldd'):
            ret = os.system("ldd %s > %s" % (do_readline, tmpfile))
        else:
            ret = 256
        if ret >> 8 == 0:
            with open(tmpfile) as fp:
                for ln in fp:
                    if 'curses' in ln:
                        readline_termcap_library = re.sub(
                            r'.*lib(n?cursesw?)\.so.*', r'\1', ln
                        ).rstrip()
                        break
                    # termcap interface split out from ncurses
                    if 'tinfo' in ln:
                        readline_termcap_library = 'tinfo'
                        break
        if os.path.exists(tmpfile):
            os.unlink(tmpfile)
    # Issue 7384: If readline is already linked against curses,
    # use the same library for the readline and curses modules.
    if 'curses' in readline_termcap_library:
        curses_library = readline_termcap_library
    elif compiler.find_library_file(lib_dirs, 'ncursesw'):
        curses_library = 'ncursesw'
    elif compiler.find_library_file(lib_dirs, 'ncurses'):
        curses_library = 'ncurses'
    elif compiler.find_library_file(lib_dirs, 'curses'):
        curses_library = 'curses'

    # Curses support, requiring the System V version of curses, often
    # provided by the ncurses library.
    curses_defines = []
    curses_includes = []
    panel_library = 'panel'
    if curses_library == 'ncursesw':
        curses_defines.append(('HAVE_NCURSESW', '1'))
        curses_includes.append('/usr/include/ncursesw')
        # Bug 1464056: If _curses.so links with ncursesw,
        # _curses_panel.so must link with panelw.
        panel_library = 'panelw'
        if host_platform == 'darwin':
            # On OS X, there is no separate /usr/lib/libncursesw nor
            # libpanelw.  If we are here, we found a locally-supplied
            # version of libncursesw.  There should be also be a
            # libpanelw.  _XOPEN_SOURCE defines are usually excluded
            # for OS X but we need _XOPEN_SOURCE_EXTENDED here for
            # ncurses wide char support
            curses_defines.append(('_XOPEN_SOURCE_EXTENDED', '1'))
    elif host_platform == 'darwin' and curses_library == 'ncurses':
        # Building with the system-suppied combined libncurses/libpanel
        curses_defines.append(('HAVE_NCURSESW', '1'))
        curses_defines.append(('_XOPEN_SOURCE_EXTENDED', '1'))

    if curses_library.startswith('ncurses'):
        curses_libs = [curses_library]
        exts.append( Extension('_curses', ['src/_cursesmodule.c'],
                               include_dirs=curses_includes,
                               define_macros=curses_defines,
                               libraries = curses_libs) )
    elif curses_library == 'curses' and host_platform != 'darwin':
            # OSX has an old Berkeley curses, not good enough for
            # the _curses module.
        if (compiler.find_library_file(lib_dirs, 'terminfo')):
            curses_libs = ['curses', 'terminfo']
        elif (compiler.find_library_file(lib_dirs, 'termcap')):
            curses_libs = ['curses', 'termcap']
        else:
            curses_libs = ['curses']

        exts.append( Extension('_curses', ['src/_cursesmodule.c'],
                               define_macros=curses_defines,
                               libraries = curses_libs) )

    # If the curses module is enabled, check for the panel module
    if (len(exts) and
        compiler.find_library_file(lib_dirs, panel_library)):
        exts.append( Extension('_curses_panel', ['src/_curses_panel.c'],
                               include_dirs=curses_includes,
                               define_macros=curses_defines,
                               libraries = [panel_library] + curses_libs) )

    return exts

with tempfile.TemporaryDirectory() as tmpdir:
    exts = configure_ext(tmpdir)

if len(exts) == 0:
    print("Unable to configure C extensions", file=sys.stderr)
    sys.exit(1)

setup(name='newcurses',
      version='0.0.1',
      description="A forked version of Python 3's curses package.",
      url="https://bitbucket.org/akuchling/newcurses/",
      author="Andrew Kuchling",
      author_email="curses@msg.amk.ca",
      license="Python",
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'Programming Language :: Python :: 3',
          'Environment :: Console :: Curses',
          'Topic :: Terminals',
          ],
      packages=['curses'],
      ext_modules=exts,
)
