.. newcurses module documentation master file, created by
   sphinx-quickstart on Tue Jul 21 16:55:56 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to newcurses module's documentation!
============================================

Contents:

.. toctree::
   :maxdepth: 2

   curses
   curses.ascii
   curses.panel
   howto


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
