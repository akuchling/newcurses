
* Copy code from Python 3.5 (or from default?).

* Create Sphinx project containing the module documentation and the HOWTO.

* Go through bugs.python.org for relevant patches, and apply them.

  issues to incorporate: https://pypi.python.org/pypi/curses_ex/0.3

* Have Windows build support.

* Better testing.

  * Pick some applications from PyPI to test against.

* Test Unicode support more completely, and fix any bugs that turn up.

* Are there packages such as urwid or blessings that should be incorporated
  or that can be tied to?
