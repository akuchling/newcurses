newcurses: Enhanced curses support for Python
=============================================

This is a forked version of Python 3's curses package.

The goals of the project are:

* To include more features by incorporating patches that are too
  extensive or risky for the Python stdlib version.

* To have faster development and more frequent releases because the
  package isn't tied to the Python 3.x release cycle, and because a
  bad release can be quickly fixed.

* To be compatible with the Python 3.x curses package.  All code that
  works with the Python stdlib package *must* also work with
  newcurses; if it doesn't, that is a bug in newcurses.

* To leave the door open to replacing the Python stdlib package, once
  newcurses has proven itself to be reliable.

Issue tracker:
https://bitbucket.org/akuchling/newcurses/issues

Wiki:
https://bitbucket.org/akuchling/newcurses/wiki/Home


For questions or comments, please write to <curses@msg.amk.ca>.

Andrew Kuchling
curses@msg.amk.ca
